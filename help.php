<!DOCTYPE html>
<html dir="rtl" lang="fa-IR">
<head>
    <meta charset="UTF-8">
    <title>راهنمای وبسرویس <?php echo $route; ?></title>
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css"
          integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If" crossorigin="anonymous">
    <style>
        body {
            font-family: tahoma;
        }

        .container {
            direction: rtl;
        }

        pre {
            direction: ltr;
            text-align: left;
        }

        .maindiv {
            width: 70%;
            margin: auto;

        }

        .leftdiv {
            width: 30%;
            margin: auto;
            background-color: azure;
            padding: 20px;
            border: 1px dashed aqua;
            border-radius: 5px;

        }

        pre {
            background-color: azure;
            padding: 20px;
            border: 1px dashed aqua;
            border-radius: 10px;
        }

        .leftmenu .list-group-item {
            text-align: left;
        }

        .list-group .active a {
            color: white;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <!-- As a heading -->
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1">MasterERP</span>
    </nav>

    <div class="row" style="margin-top:10px;">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">وبسرویس <?php echo $route; ?></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-8">
            <?php
            $homepage = file_get_contents($route . '.txt');
            echo $homepage;

            ?>
        </div>

        <div class="col-sm-4 leftmenu">

            <?php
            function getDirContents($dir, &$results = array())
            {
                $files = scandir($dir);

                foreach ($files as $key => $value) {
                    $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
                    if (!is_dir($path)) {
                        if (strpos($path, '.txt') !== false) {
                            $item_path = str_replace("/home/masterka/erp/api/1.0.0/", "", $path);
                            $results[] = str_replace(".txt", "", $item_path);
                        }
                    } else if ($value != "." && $value != "..") {
                        getDirContents($path, $results);
                        //$results[] = $path;
                    }
                }

                return $results;
            }

            function getCategories($dirContent)
            {
                $retArr = array();
                foreach ($dirContent as $item) {
                    $mycatitem = explode("/", $item)[0];
                    if (!in_array($mycatitem, $retArr)) {
                        $retArr[] = $mycatitem;
                    }
                }
                return $retArr;
            }

            function startsWith($string, $startString)
            {
                $len = strlen($startString);
                return (substr($string, 0, $len) === $startString);
            }

            //echo "<pre>";
            $dirContent = getDirContents('.');
            //echo "</pre>";
            $categories = getCategories($dirContent);
            ?>

            <ul class="list-group">

                <?php
                foreach ($categories as $cat) {
                    echo '<li class="list-group-item disabled"><a >' . $cat . '</a></li>';

                    foreach ($dirContent as $item) {
                        if (startsWith($item, $cat . "/"))
                            echo '<li class="list-group-item ' . ($item == $route ? "active" : "") . '" ><a style="margin-left:20px;" href="https://api.erp.masterkala.com/1.0.0/?route=' . $item . '&help=1">' . $item . '</a></li>';
                    }
                }
                ?>
            </ul>

        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"
        integrity="sha384-a9xOd0rz8w0J8zqj1qJic7GPFfyMfoiuDjC9rqXlVOcGO/dmRqzMn34gZYDTel8k"
        crossorigin="anonymous"></script>

</body>
</html>

