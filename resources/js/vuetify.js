import Vue from 'vue';
import Vuetify, {
    VMain,
    VCard,
    VToolbar,
    VNavigationDrawer,
    VList,
    VSkeletonLoader,
    VListItem,
    VListItemAction,
    VIcon,
    VListItemTitle,
    VApp,
    VAppBar,
    VToolbarTitle,
    VAppBarNavIcon,
    VContent,
    VContainer,
    VRow,
    VCardSubtitle,
    VCol,
    VTextarea,
    VBtn,
    VListItemContent,
    VTextField,
    VSpacer,
    VListItemAvatar,
    VSnackbar,
    VMenu,
    VAvatar,
    VListItemGroup,
    VProgressLinear,
    VChip,
    VChipGroup,
    VSimpleTable,
    VListItemIcon,
    VListItemSubtitle,
    VFlex,
    VProgressCircular,
    VItemGroup,
    VWindow,
    VSelect,
    VHover,
    VRating,
    VDialog,
    VCardText,
    VAlert,
    VCardTitle,
    VBadge,
    VCardActions,
    VItem,
    VWindowItem,
    VDivider,
    VCarousel,
    VCarouselItem,
    VSlideGroup,
    VSlideItem,
    VLayout,
    VSheet,
    VImg,
    VSlider,
    VFooter,
    VTab,
    VTabItem,
    VTabs,
    VTabsItems,
    VExpansionPanel,
    VExpansionPanels,
    VExpansionPanelHeader,
    VExpansionPanelContent,
    VBreadcrumbs,
    VBreadcrumbsItem,
    VCombobox,
    VForm,
    VFileInput,
    VSpeedDial,
    VSwitch,
    VExpandXTransition,
    VSlideXReverseTransition,
    VSlideXTransition,
    VSlideYReverseTransition,
    VSlideYTransition,
    VCheckbox,
    VTooltip,VExpandTransition,VRadioGroup,VRadio,VPagination,VSubheader
} from 'vuetify/lib'

import {Ripple, Intersect, Scroll, Resize} from 'vuetify/lib/directives'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify, {
    rtl: true,
    icons: {
        iconfont: 'mdi',
    },
    components: {
        VMain,
        VCard,
        VChip,
        VChipGroup,
        VToolbar,
        VNavigationDrawer,
        VAlert,
        VList,
        VListItem,
        VListItemAction,
        VIcon,
        VSkeletonLoader,
        VItem,
        VWindowItem,
        VListItemTitle,
        VApp,
        VTextarea,
        VSpacer,
        VAppBar,
        VProgressCircular,
        VSlider,
        VToolbarTitle,
        VSnackbar,
        VAppBarNavIcon,
        VContent,
        VContainer,
        VSheet,
        VRating,
        VRow,
        VCol,
        VListItemGroup,
        VBtn,
        VListItemContent,
        VTextField,
        VBadge,
        VItemGroup,
        VWindow,
        VSlideGroup,
        VSlideItem,
        VListItemAvatar,
        VListItemIcon,
        VListItemSubtitle,
        VMenu,
        VAvatar,
        VProgressLinear,
        VSimpleTable,
        VFlex,
        VSelect,
        VDialog,
        VCardText,
        VCardTitle,
        VCarousel,
        VCarouselItem,
        VFooter,
        VCardActions,
        VDivider,
        VLayout,
        VImg,
        Intersect,
        Scroll,
        VHover,
        VCardSubtitle,
        VTab,
        VTabItem,
        VTabs,
        VTabsItems,
        VExpansionPanel,
        VExpansionPanels,
        VExpansionPanelHeader,
        VExpansionPanelContent,
        VBreadcrumbs,
        VBreadcrumbsItem,
        VCombobox,
        VForm,
        VFileInput,
        VSpeedDial,
        VSwitch,
        VExpandXTransition,
        VSlideXReverseTransition,
        VSlideXTransition,
        VSlideYReverseTransition,
        VSlideYTransition,
        VCheckbox,
        VTooltip,VExpandTransition,VRadioGroup,VRadio,VPagination,VSubheader
    },
    directives: {
        Ripple, Intersect, Scroll, Resize
    }
});

const opts = {
    rtl: true,
    breakpoint: {scrollbarWidth: 5},
    theme: {
        themes: {
            light: {
                primary: '#3498db',
                secondary: '#b0bec5',
            },
            dark: {
                primary: '#3498db',
                secondary: '#FFF'
            }
        },
    },
};

export default new Vuetify(opts);
