require('./bootstrap');
import Vue from 'vue';
import vuetify from './vuetify';
import VueRouter from 'vue-router';
import VueNotification from '@mathieustan/vue-notification';

import home from "./components/home";
import login from "./components/login";

Vue.component('index', require('./components/index.vue').default);
Vue.use(VueNotification, {
    theme: {
        colors: {
            success: '#05c46b',
            darkenSuccess: '#2d8e36',
            info: '#5d6a89',
            darkenInfo: '#535f7b',
            warning: '#f8a623',
            darkenWarning: '#f69a07',
            error: '#e74c3c',
            darkenError: '#ff245f',
            offline: '#ff4577',
            darkenOffline: '#ff245f',
        },
        boxShadow: '5px 5px 10px rgba(0,0,0,0.3)',
    },
    breakpoints: {
        0: {
            bottom: true,
        },
        480: {
            top: true,
            right: true,
        },
    },
});
Vue.use(VueRouter);

Vue.mixin({
    data: () => ({
        apiLoading: false,
    }),
    methods: {
        toggleNightMode() {
            this.$vuetify.theme.dark = !this.$vuetify.theme.dark;
            localStorage.theme = this.$vuetify.theme.dark ? 'dark' : 'light';
        },
        goto: function (route, query = {}) {
            if (this.$route.fullPath !== '/' + route) {
                router.push({path: "/" + route, query: query})
            }
        },
        goReplace(route) {
            router.replace({path: "/" + route})
        },
        preparingRouteGuard() {
            router.beforeEach((to, from, next) => {
                if (to.matched.some(record => record.meta.requiresAuth) && this.$root.user === null) {
                    next({path: '/login'});
                } else {
                    next();
                }
            });
        },
        sendToLogin() {
            this.$root.user = null
            localStorage.user = null;
            localStorage.metoken = null;
            this.goto('login');
            this.$root.$emit('userChange', null);
        },
        error403(message) {
            this.goReplace('')
            this.$root.$emit('error', message);
        },
        requestHTTP(method, url, object, success, failed, configs) {
            let config = {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                params: {}
            };
            if (configs !== undefined && configs.headers !== undefined) {
                config = configs;
            }
            if (localStorage.metoken !== undefined && localStorage.metoken !== null) {
                window.axios.defaults.headers.common['Metoken'] = localStorage.metoken;
            }
            this.apiLoading = true;
            if (method === 'post') {
                axios.post(url, object, config)
                    .then(({data}) => {
                        try {
                            success(data);
                        } catch (ex) {
                        }
                    })
                    .catch(({response}) => {
                        if (response.status === 401) {
                            return this.sendToLogin()
                        } else if (response.status === 403) {
                            this.error403(response.data.message);
                        }
                        failed(response);
                    })
                    .finally(() => this.apiLoading = false);

            } else if (method === 'get') {
                axios.get(url, object, config)
                    .then(({data}) => {
                        try {
                            success(data);
                        } catch (ex) {
                        }
                    })
                    .catch(({response}) => {
                        if (response.status === 401) {
                            return this.sendToLogin()
                        } else if (response.status === 403) {
                            this.error403(response.data.message);
                        }
                        failed(response);
                    })
                    .finally(() => this.apiLoading = false);
            } else if (method === 'put') {
                axios.put(url, object, config)
                    .then(({data}) => {
                        try {
                            success(data);
                        } catch (ex) {
                        }
                    })
                    .catch(({response}) => {
                        if (response.status === 401) {
                            return this.sendToLogin()
                        } else if (response.status === 403) {
                            this.error403(response.data.message);
                        }
                        failed(response);
                    })
                    .finally(() => this.apiLoading = false);
            } else if (method === 'delete') {
                axios.delete(url, object, config)
                    .then(({data}) => {
                        try {
                            success(data);
                        } catch (ex) {
                        }
                    })
                    .catch(({response}) => {
                        if (response.status === 401) {
                            return this.sendToLogin()
                        } else if (response.status === 403) {
                            this.error403(response.data.message);
                        }
                        failed(response);
                    })
                    .finally(() => this.apiLoading = false);
            }
        },
    }
});

const routes = [
    {path: '/login', component: login},
    {path: '*', component: home, meta: {requiresAuth: true}}
];
const router = new VueRouter({
    routes
});
new Vue({
    vuetify,
    router

}).$mount('#app');
