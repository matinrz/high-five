<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Content-Language" content="fa"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>High Five</title>

    <script src="public/js/app.js" defer></script>
    <link href="public/css/app.css" rel="stylesheet">
</head>

<body>
<div id="app" style="height: 100vh">
    <index></index>
</div>
</body>
</html>


