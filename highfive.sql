-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2021 at 10:21 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `highfive`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `sender_name` varchar(50) NOT NULL,
  `send_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `thread_id`, `user_id`, `sender_id`, `text`, `sender_name`, `send_date`) VALUES
(1, 1, 2, 1, 'salam salam', 'asdasd', '2021-06-10 15:23:30'),
(2, 1, 1, 2, 'سلام خوبی ؟', 'matinrezaee', '2021-06-12 17:22:08'),
(14, 1, 1, 2, 'fadat', 'matinrezaee', '2021-06-14 16:41:39'),
(15, 1, 1, 2, 'merc', 'matinrezaee', '2021-06-14 16:42:33'),
(16, 1, 1, 2, 'asd', 'matinrezaee', '2021-06-15 18:18:26'),
(17, 1, 2, 1, 'asdasdasd', 'asdasd', '2021-06-15 18:20:32'),
(18, 0, -1, 1, 'asd', 'asdasd', '2021-06-18 15:38:14'),
(19, 29, -1, 1, 'asd', 'asdasd', '2021-06-18 15:38:52'),
(20, 0, -1, 1, '', 'asdasd', '2021-06-19 22:34:12'),
(21, 0, -1, 1, 'asd', 'asdasd', '2021-06-19 22:34:36'),
(22, 0, -1, 1, 'asd', 'asdasd', '2021-06-19 22:34:42'),
(23, 29, -1, 1, '.', 'asdasd', '2021-06-19 22:41:24'),
(24, 29, -1, 1, '1', 'asdasd', '2021-06-19 22:45:53'),
(25, 29, -1, 1, '123', 'asdasd', '2021-06-19 22:46:43'),
(26, 29, -1, 1, '123', 'asdasd', '2021-06-19 22:46:45'),
(27, 29, -1, 1, '123', 'asdasd', '2021-06-19 22:46:46'),
(28, 29, -1, 1, '123', 'asdasd', '2021-06-19 22:46:46'),
(29, 29, -1, 1, '123', 'asdasd', '2021-06-19 22:46:47'),
(30, 29, -1, 1, '123', 'asdasd', '2021-06-19 22:46:48'),
(31, 29, -1, 1, '123', 'asdasd', '2021-06-19 22:46:48'),
(32, 29, -1, 1, '123', 'asdasd', '2021-06-19 22:46:48');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `thread_id`, `admin_id`) VALUES
(2, 'گروه مهاجر', 26, 2),
(3, 'اتاق تست', 27, 1),
(4, 'تست اتاق', 28, 1),
(5, 'تست نهایی اتاق', 29, 1);

-- --------------------------------------------------------

--
-- Table structure for table `room_members`
--

CREATE TABLE `room_members` (
  `id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE `threads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `target_type` varchar(20) DEFAULT NULL,
  `target_name` varchar(50) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `user_id`, `user_name`, `target_type`, `target_name`, `target_id`) VALUES
(1, 1, NULL, 'user', 'asdasd', 2),
(26, 2, NULL, 'room', 'گروه مهاجر', -1),
(27, 1, NULL, 'room', 'اتاق تست', -1),
(28, 1, NULL, 'room', 'تست اتاق', -1),
(29, 1, NULL, 'room', 'تست نهایی اتاق', -1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `password` varchar(60) NOT NULL,
  `user_agent` text DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `token` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `password`, `user_agent`, `username`, `token`) VALUES
(1, 'asdasd', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 'asdasd', 'xwZsEFtA89TeTWDvbpPkdGuXh6W7jNtjpdDEz7ehtELxItetqZ'),
(2, 'matinrezaee', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', 'matinrezaee', '9LugA8EKlk9gatnCkR77bs1nSoum7P2U4Z4FEKQfuRiQQEKm8V');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rooms_id_uindex` (`id`);

--
-- Indexes for table `room_members`
--
ALTER TABLE `room_members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `room_members_id_uindex` (`id`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_pk` (`username`),
  ADD KEY `users_id_index` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `room_members`
--
ALTER TABLE `room_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
