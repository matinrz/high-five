<?php

class API extends BaseAPI
{
    public function run()
    {
        if (is_null($this->raw)) {
            $this->response(['message' => 'اطلاعات ناقص ارسال شده است']);
        }
        $qObj = array(
            "table" => "messages",
            "values" => array(
                "user_id" => -1,
                "thread_id" => $this->raw['thread_id'],
                "sender_id" => $this->user['id'],
                "sender_name" => $this->user['username'],
                "send_date" => date("Y-m-d H:i:s"),
                "text" => $this->raw['text']));
        $message = $this->db->insert($qObj);
        if ($message) $this->response(['message' => 'پیام در اتاق ارسال شد']);
        else $this->response(["message" => "مشکلی پیش آمده"], 503);
    }
}