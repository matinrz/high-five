<?php

class API extends BaseAPI
{
    public function run()
    {
        if (is_null($this->raw)) {
            $this->response(['message' => 'اطلاعات ناقص ارسال شده است']);
        }
        $target = $this->db->runselect("SELECT * FROM users where id='" . $this->raw['user_id'] . "'");
        if (!$target)  $this->response($this->db->error); else $target = $target[0];
        $checkThread = $this->db->runselect("SELECT * FROM threads
            where user_id='" . $this->raw['sender_id'] . "'" . " AND target_id='" . $target['id'] . "'"
            . " OR user_id='" . $target['id'] . "'" . " AND target_id='" . $this->raw['sender_id'] . "'"); // check exist thread

        $thread = null;
        if (empty($checkThread)) {
            $qObj = array(
                "table" => "threads",
                "values" => array(
                    "user_id" => $this->user['id'],
                    "user_name" => $this->user['username'],
                    "target_id" => $target['id'],
                    "target_type" => "user",
                    "target_name" => $target['username']));
            $thread = $this->db->insert($qObj);
            if ($thread) {
                $qObj = array(
                    "table" => "messages",
                    "values" => array(
                        "user_id" => $target['id'],
                        "thread_id" => $this->db->insert_id,
                        "sender_id" => $this->user['id'],
                        "sender_name" => $this->user['username'],
                        "send_date" => date("Y-m-d H:i:s"),
                        "text" => $this->raw['text']));
                $message = $this->db->insert($qObj);
            } else  $this->response($this->db->error);
             $this->response(['message' => $message, 'status' => 'New Thread']);

        } else {
            $qObj = array(
                "table" => "messages",
                "values" => array(
                    "user_id" => $target['id'],
                    "thread_id" => $checkThread[0]['id'],
                    "sender_id" => $this->user['id'],
                    "sender_name" => $this->user['username'],
                    "send_date" => date("Y-m-d H:i:s"),
                    "text" => $this->raw['text']));
            $message = $this->db->insert($qObj);
            if ($message) $this->response($message);
            else $this->response($this->db->error);

        }
    }
}