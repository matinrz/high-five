<?php

class API extends BaseAPI
{
    public function run()
    {
        $qObj = array(
            "table" => "threads",
            "values" => array(
                "user_id" => $this->user['id'],
                "target_id" => -1,
                "target_type" => "room",
                "target_name" => $this->raw['name']));
        $thread = $this->db->insert($qObj);
        if ($thread) {
            $qObj = array(
                "table" => "rooms",
                "values" => array(
                    "name" => $this->raw['name'],
                    "admin_id" => $this->user['id'],
                    "thread_id" => $this->db->insert_id
                ),
            );
            $room = $this->db->insert($qObj);

            $qObj = array(
                "table" => "room_members",
                "values" => array(
                    "room_id" => $this->user['id'],
                    "user_id" => $this->db->insert_id
                ),
            );
            $user_add_to_room = $this->db->insert($qObj);
            if ($room && $user_add_to_room) $this->response(['message' => 'اتاق با موفقیت ساخته شد']);
        } else
            $this->response([
                "message" => "گروه ساخته نشد",
                "error" => $this->db->error
            ], 422);
    }
}