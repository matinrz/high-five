<?php
// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'highfive');
define('DB_PORT', '3306');

define('CG_BARCODE_MIN', '1000');
define('CG_BARCODE_MAX', '230000');
define('LSAT_APP_V', '2.7');
define('DEBUG_IP', '37.255.203.46');
