<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set('Asia/Tehran');

include_once 'config.php';

include_once 'service.php';

if (!isset($_GET['route'])) {
    include_once 'home.php';
    exit();
}

$route = $_GET['route'];

if (isset($_GET['help'])) {
    include_once 'help.php';
    exit();
}


if (file_exists($route . '.php')) {
    include_once $route . '.php';

    $api = new API;
    $api->run();
} else {
    http_response_code(404);
    exit();
}


class BaseAPI
{
    public $db;
    public $jsonObj;
    public $raw;
    public $alert;
    public $user;
    public $metoken;
    public $appverion;
    public $is_debug = false;

    function __construct()
    {
        $this->db = new BaseDB;
        $this->readRaw();
        $this->prepareAlert();


        $header = apache_request_headers();

        $this->appverion = isset($header['appverion']) ? $header['appverion'] : '0';

        if (!$this->tokenValidator()) {
            $this->response($this->alert, 401);
            exit();
        }


        if ($_SERVER['REMOTE_ADDR'] == '37.255.203.46' || $_SERVER['REMOTE_ADDR'] == '37.255.210.1') {
            $this->is_debug = true;
        }
    }

    function __destruct()
    {
        $this->db = null;
    }

    public function getlicenceresponse($licence)
    {
        $rescode = md5($licence);
        $rescode = preg_replace('/\D/', '', $rescode);
        $rescode = substr($rescode, 0, 5);
        $rescodeInt = (int)$rescode;
        $rescodeInt = ($rescodeInt * 50) + 310;
        $rescode = $rescodeInt . "";
        $rescode = md5($rescode);
        $rescode = preg_replace('/\D/', '', $rescode);
        return $rescode;
    }

    private function readRaw()
    {
        $rawdata = file_get_contents("php://input");
        $this->raw = json_decode($rawdata, true);
        if (isset($_GET['export']) && isset($this->raw["limit"])) {
            $this->raw["start"] = "0";
            $this->raw["limit"] = "100000";
        }
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function prepareAlert()
    {
        $this->alert = array();
        $this->alert["success"] = true;
        $this->alert["errors"] = array();
        $this->alert["message"] = "";
    }

    public function response($obj, $statusCode = 200)
    {
        if (isset($this->alert["errors"]) && sizeof($this->alert["errors"]) > 0) {
            if ($statusCode == 200)
                $statusCode = 400;
            if ($this->alert["message"] == "")
                $this->alert["message"] = $this->getFirstError();
            $obj = $this->alert;
        }

        if (isset($_GET['export']) && isset($obj["list"])) {
            $export = new ExportHelper();
            $tempFileUrl = $export->export($_GET['export'], $obj["list"]);
            $obj = array();
            $obj["fileurl"] = $tempFileUrl;
        }


        $encoded = json_encode($obj, 256);
        $unescaped = preg_replace_callback('/\\\\u(\w{4})/', function ($matches) {
            return html_entity_decode('&#x' . $matches[1] . ';', ENT_COMPAT, 'UTF-8');
        }, $encoded);


        $this->cors();

        header('Content-Type: application/json');
        http_response_code($statusCode);
        echo $unescaped;

        $api = null;
        exit();
    }

    private function cors()
    {

        // Allow from any origin
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
            // you want to allow, and if so:
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                // may also be using PUT, PATCH, HEAD etc
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

            exit(0);
        }
    }

    public function addError($key, $value)
    {
        $this->alert["success"] = false;
        $this->alert["errors"][$key] = $value;

    }

    public function getFirstError()
    {
        foreach ($this->alert["errors"] as $key => $value) {
            return $value;
        }
    }

    public function setMessage($msg)
    {
        $this->alert["message"] = $msg;
    }

    public function setMessageId($id)
    {
        $this->alert["id"] = $id;
    }

    public function v_empty(&$input)
    {
        if (!isset($input)) return true;
        if (is_array($input)) return true;
        return (trim($input) == "");
    }

    public function contain($fullmessage, $search)
    {
        if (strpos($fullmessage, $search) !== false) {
            return true;
        }
        return false;
    }

    public function success()
    {
        return $this->alert["success"];
    }

    public function getUserIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    private function tokenValidator()
    {
        $isValid = false;

        if ($this->contain($_GET['route'], "authenticate/")) {
            $isValid = true;
        } else {
            $header = apache_request_headers();
            $this->metoken = isset($header['metoken']) ? $header['metoken'] : '1';
            $this->metoken = isset($header['Metoken']) ? $header['Metoken'] : $this->metoken;
            $users = $this->db->runselect("SELECT id,username FROM users WHERE user_agent='" . $this->db->escape($_SERVER['HTTP_USER_AGENT']) . "' AND token='" . $this->db->escape($this->metoken) . "';");

            if (sizeof($users) > 0) {
                $this->user = $users[0];
                $isValid = true;
            } else {
                $isValid = false;
                $this->addError("token", "این وبسرویس نیاز به ارسال توکن دارد.");
                $this->setMessage("احراز هویت انجام نشده است.");
            }

        }

        return $isValid;
    }

}


class BaseDB
{
    public $insert_id = 0;
    private $connection;
    public $error = "";

    function __construct()
    {
        $this->connection = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE) or die("Error " . mysqli_error($connection));
    }

    function __destruct()
    {
        mysqli_close($this->connection);
    }

    public function query($sql)
    {
        $sql = str_replace("ي", "ی", $sql);
        $sql = str_replace("ك", "ک", $sql);
        $result = mysqli_query($this->connection, $sql) or $this->error = mysqli_error($this->connection);
        $this->insert_id = $this->connection->insert_id;
        return $result;
    }

    public function runselect($sql)
    {
        $resultQ = $this->query($sql);
        $arrRes = array();
        while ($row = mysqli_fetch_assoc($resultQ))
            $arrRes[] = $row;
        return $arrRes;
    }

    public function runselectkeyvalue($sql)
    {
        $resultConfig = $this->query($sql);
        $arrConfig = array();
        while ($row = mysqli_fetch_assoc($resultConfig))
            $arrConfig[$row['key']] = $row['value'];
        return $arrConfig;
    }

    public function insert($queryObject)
    {
        $sql = "";
        foreach ($queryObject["values"] as $key => $value) {
            $valueQ = ($value == null ? "''" : "'" . $this->escape($value) . "'");
            if ($value == "NULL") $valueQ = "NULL";

            $sql .= ($sql == "" ? "" : ",") . $key . " = " . $valueQ;
        }
        $sql = "INSERT INTO " . $queryObject["table"] . " SET " . $sql . ";";

        return $this->query($sql);
    }

    public function update($queryObject)
    {
        $sql = "";
        foreach ($queryObject["values"] as $key => $value) {
            $valueQ = ($value == null ? "''" : "'" . $this->escape($value) . "'");
            if ($value == "NULL") $valueQ = "NULL";

            $sql .= ($sql == "" ? "" : ",") . $key . " = " . $valueQ;
        }

        $where = "";
        foreach ($queryObject["where"] as $key => $value) {
            if ($value == "NULL")
                $where .= ($where == "" ? "" : " AND ") . $key . " IS NULL ";
            else
                $where .= ($where == "" ? "" : " AND ") . $key . " = '" . $this->escape($value) . "' ";
        }
        $sql = "UPDATE " . $queryObject["table"] . " SET " . $sql . " WHERE " . $where . ";";
        return $this->query($sql);
    }

    public function delete($queryObject)
    {
        $where = "";
        foreach ($queryObject["where"] as $key => $value) {
            $where .= ($where == "" ? "" : " AND ") . $key . " = '" . $this->escape($value) . "' ";
        }
        $sql = "DELETE FROM " . $queryObject["table"] . " WHERE " . $where . ";";
        return $this->query($sql);
    }


    public function getWhereQuery($filters)
    {
        $wArr = array();
        foreach ($filters as $key => $value) {
            $newKey = str_replace("__", ".", $key);
            if ($value == "NULL") {
                $wArr[] = $newKey . " IS NULL";
            } else if ($value == "NOTNULL") {
                $wArr[] = $newKey . " IS NOT NULL";
            } else {
                $wArr[] = $newKey . " " . ($this->contain($value, "%") ? "LIKE" : "=") . " '" . $this->escape($value) . "'";
            }
        }
        if (sizeof($wArr) > 0)
            return implode(" AND ", $wArr);
        else
            return " 1=1 ";

    }

    public function getSortLimitQuery($sort, $order, $start, $limit)
    {
        $resQ = "";
        if ($sort != "") {
            $resQ .= " ORDER BY " . $sort . " " . $order;
        }
        if ($limit > 0) {
            $resQ .= " LIMIT " . $start . "," . $limit;
        }
        return $resQ;
    }

    public function escape($str)
    {
        return mysqli_real_escape_string($this->connection, $str);
    }

    public function getLastId()
    {
        return $this->insert_id;
    }

    public function getIdsAsString($arr, $idKey)
    {
        $retArr = array();
        foreach ($arr as $item) {
            $retArr[] = $item[$idKey];
        }
        return implode(",", $retArr);
    }

    public function contain($fullmessage, $search)
    {
        if (strpos($fullmessage, $search) !== false) {
            return true;
        }
        return false;
    }
}

?>