<?php

class API extends BaseAPI
{
    public function run()
    {
        if (is_null($this->raw)) {
            $this->response(['message' => 'اطلاعات ناقص ارسال شده است']);
        }

        $resObj = $this->db->runselect("SELECT id,username FROM users where username='" . $this->raw['username'] . "'");
        if ($resObj) { // agar user find shod = login kon
            $token = $this->generateRandomString(50);
            $qObj = array(
                "table" => "users",
                "values" => array(
                    "token" => $token,
                    "user_agent" => $this->db->escape($_SERVER['HTTP_USER_AGENT'])
                ),
                "where" => array(
                    "username" => $this->raw['username']
                )
            );
            $this->db->update($qObj);
            $this->response([
                'message' => 'عملیات با موفقیت انجام شد',
                'user' => $resObj,
                'token' => $token,
            ]);
        } else {// agar user find na shod = register kon
            $qObj = array(
                "table" => "users",
                "values" => array(
                    "username" => $this->raw['username'],
                    "password" => $this->raw['password'],
                    "token" => $this->generateRandomString(50),
                    "user_agent" => get_browser(null, true)));
            $res = $this->db->insert($qObj);
            if ($res)
                $this->response([
                    'message' => 'ثبت نام با موفقیت انجام شد',
                    'status' => 'success'
                ]);
            else  $this->response(['message' => 'مشکلی در ثبت نام شما به وجود آمده است']);
        }
    }
}
