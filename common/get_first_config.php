<?php

class API extends BaseAPI
{
    public function run()
    {
        $resObj = array();

        $resObj["main_tables"] = $this->getMainTables();
        $resObj["inside_ids"] = $this->getInsideIds();
        $resObj["inside_classes"] = $this->getInsideClasses();
        $this->response($resObj);
    }

    public function getMainTables()
    {
        $list = array();
        $list[] = "IndexTable15";
        $list[] = "IndexTable16";
        $list[] = "IndexTable17";
        $list[] = "IndexTable18";
        $list[] = "IndexTable26";
        $list[] = "IndexTable28";
        $list[] = "IndexTable29";
        $list[] = "IndexTable1";
        $list[] = "IndexTable9";
        $list[] = "IndexTable19";
        $list[] = "IndexTable24";
        $list[] = "IndexTable22";
        $list[] = "IndexTable12";
        $list[] = "IndexTable2";
        $list[] = "IndexTable6";

        return $list;
    }

    public function getInsideIds()
    {
        $list = array();
        $list[] = "n_imph";
        $list[] = "n_impm";

        return $list;
    }

    public function getInsideClasses()
    {
        $list = array();
        $list[] = "n_price";
        $list[] = "n_pricepercent";
        $list[] = "n_last";
        $list[] = "n_pclosingpercent";

        return $list;
    }

}

?>
